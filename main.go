package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

func main() {
	router := mux.NewRouter()
	godotenv.Load(".env")

	host := os.Getenv("HOST")
	port := os.Getenv("PORT")

	if host == "" || port == "" {
		log.Fatalf("HOST and PORT need to be in environment")
		return
	}

	router.HandleFunc("/hello",
		func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("hello"))
		},
	).Methods("GET")

	url := fmt.Sprintf("%s:%s", host, port)
	log.Printf("Starting app : %s", url)
	if err := http.ListenAndServe(url, router); err != nil {
		log.Fatalf("Failed starting server | %s", err.Error())
		return
	}
}
